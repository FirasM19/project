#include <iostream>
using namespace std; struct node{
	int data;
	struct node* next;
};
void Push(struct node** headRef, int data){
	struct node* newNode = new struct node;
	newNode->data = data;
	newNode->next = *headRef;
	*headRef = newNode;
}
void Print(struct node* head){
	struct node* current = head;
		while(current!= NULL){
			cout << current->data << endl;
			current = current->next;
		}
}
int Length(struct node* head){
	struct node* current = head;
	int counter = 0;
		while(current!= NULL){
			//cout << current->data << endl;
			counter++;
			current = current->next;
		}
	return counter;
}
void PrintRecursive(struct node* head){
	if(head == NULL) return;
	cout << head->data << endl;
	PrintRecursive(head->next);
}
void AddToEnd(struct node** headRef, int data){
	struct node* current = *headRef;
	if(current == NULL){
		Push(headRef, data);
	}else{
		while(current->next != NULL){
			current = current->next;
		}
		Push(&(current->next), data);
	}
}
struct node* FindFromTheEnd(struct node* head, int k){
	struct node* current = head;
	struct node* runner = head;
	if(k>Length(head)){
		return NULL;
	}
	for(int i=0; i<k; i++){
		runner = runner->next;
	}
	while(runner != NULL){
		current = current->next;
		runner = runner->next;
	}
	return current;
}
void InsertSorted(struct node** headRef, int data){
	struct node* newNode = new struct node;
	newNode->data = data;
	struct node* current = *headRef;
	if(current == NULL || (*headRef)->data >= data){
		newNode->next = *headRef;
		*headRef = newNode;
	}else{
		while(current->next != NULL && current->next->data < 
data){
			current = current->next;
		}
		newNode->next = current->next;
		current->next = newNode;
	}
}
struct node* NewNode(int data){
	node* aNode = new struct node;
	aNode->data = data;
	aNode->next = NULL;
	return aNode;
}
struct node* CopyList(struct node* head){
	struct node* currNode = head;
	struct node* copyHead = NULL;
	struct node* copyTail = NULL;
	struct node* newNode = NULL;
	while(currNode != NULL){
		newNode = NewNode(currNode->data);
		if(copyHead == NULL){
			copyHead = newNode;
			copyTail = copyHead;
		}
		else
			copyTail->next = newNode;
		copyTail = newNode;
		currNode = currNode->next;
	}
	return copyHead;
}
bool EqualsLists(struct node* head1, struct node* head2){
	struct node* currL1 = head1;
	struct node* currL2 = head2;
	while(currL1 != NULL and currL2 != NULL){
		if(currL1->data != currL2->data)
			return false;
		currL1 = currL1->next;
		currL2 = currL2->next;
	}
	if(currL1 != NULL or currL2 != NULL)
		return false;
	return true;
}
struct node* MergeTwoSorted(struct node* head1, struct node* head2){
	struct node* mergedHead = NULL;
	struct node* mergedTail = NULL;
	struct node* newNode = NULL;
	struct node* currL1 = head1, *currL2 = head2;
	while(currL1 != NULL or currL2 != NULL){
		if(currL2 == NULL or (currL1 != NULL and currL1->data <= 
currL2->data)){
			newNode = NewNode(currL1->data);
			currL1 = currL1->next;
		}
		else{
			newNode = NewNode(currL2->data);
			currL2 = currL2->next;
		}
		if (mergedHead == NULL)
			mergedHead = newNode;
		else
			mergedTail->next = newNode;
		mergedTail = newNode;
	}
	return mergedHead;
}
void Reverse(struct node** headRef){
	struct node* prevNode = NULL;
	struct node* currNode = *headRef;
	struct node* changeNode;
	while(currNode != NULL){
		changeNode = currNode;
		currNode = currNode->next;
		changeNode->next = prevNode;
		prevNode = changeNode;
	}
	*headRef = prevNode;
}
bool IsPalindrome(struct node* head){
	struct node* reversed = CopyList(head);
	Reverse(&reversed);
	return EqualsLists(head, reversed);
}
void OddEvenRearrange(struct node** headRef){
	if(*headRef == NULL) return;
	struct node* oddTail = *headRef;
	struct node* evenHead = NULL;
	struct node* evenTail = NULL;
	struct node* currNode = (*headRef)->next;
	bool isOdd = false;
	while(currNode != NULL){
		if(isOdd){
			oddTail->next = currNode;
			oddTail = currNode;
		}
		else{
			if(evenTail == NULL)
				evenHead = currNode;
			else
				evenTail->next = currNode;
			evenTail = currNode;
		}
		currNode = currNode->next;
		isOdd = !isOdd;
	}
	oddTail->next = evenHead;
	if(evenTail != NULL)
		evenTail->next = NULL;
}
int main() {
	struct node* list1 = NULL;
	InsertSorted(&list1, 3);
	InsertSorted(&list1, 2);
	InsertSorted(&list1, 4);
	InsertSorted(&list1, 1);
	cout << "List 1:\n";
	Print(list1);
	struct node* list2 = NULL;
	InsertSorted(&list2, 2);
	InsertSorted(&list2, 5);
	InsertSorted(&list2, 4);
	InsertSorted(&list2, 2);
	InsertSorted(&list2, 6);
	cout << "\nList 2:\n";
	Print(list2);
	struct node* merged = MergeTwoSorted(list1, list2);
	cout << "\nMerged List1 and List 2:\n";
	Print(merged);
	struct node* list3 = NULL;
	AddToEnd(&list3, 8);
	AddToEnd(&list3, 10);
	AddToEnd(&list3, 11);
	AddToEnd(&list3, 12);
	AddToEnd(&list3, 14);
	AddToEnd(&list3, 15);
	cout << "\nList 3:\n";
	Print(list3);
	Reverse(&list3);
	cout << "\nReversed List 3:\n";
	Print(list3);
	struct node* list4 = NULL;
	AddToEnd(&list4, 1);
	AddToEnd(&list4, 2);
	AddToEnd(&list4, 3);
	AddToEnd(&list4, 3);
	AddToEnd(&list4, 2);
	AddToEnd(&list4, 1);
	cout << "\nList 4:\n";
	Print(list4);
	cout << "\nIs List 4 a palindrome?: ";
	if(IsPalindrome(list4))
		cout << "Yes\n";
	else
		cout << "No\n";
	struct node* list5 = NULL;
	AddToEnd(&list5, 8);
	AddToEnd(&list5, 10);
	AddToEnd(&list5, 11);
	AddToEnd(&list5, 12);
	AddToEnd(&list5, 14);
	AddToEnd(&list5, 15);
	cout << "\nList 5:\n";
	Print(list5);
	OddEvenRearrange(&list5);
	cout << "\nRearranged Odd Even List 5:\n";
	Print(list5);
	return 0;
}
